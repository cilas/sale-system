# Teste Vaga Desenvolvedor VX-CASE

[Requisitos do Projeto](README_PROJETO.md)  

Demonstração da Solução: [https://vx-sale-system.herokuapp.com//](https://vx-sale-system.herokuapp.com/) 

## Tecnologias usadas:  
Laravel  
Vue/Vuex  
Bulma  

## Instalação
 1. Clone este repositório  
 ` git clone https://gitlab.com/cilas/sale-system.git`  
 `cd sale-system`  
 2. Instale as dependencias  
 `composer install`  
 3. Configure o ambiente  
 `cp .env.example .env`  
 `php artisan key:generate`  
    - Popule o banco de dados mysql  
 `php artisan migrate --seed`  
 4. Rode o servidor  
 `php artisan serve`  


